import { contextBridge, ipcRenderer } from 'electron'
import { electronAPI } from '@electron-toolkit/preload'
import {
  Api,
  DeleteSessionInfo,
  DownloadInfo,
  DownloadProgress,
  RenameCollectionInfo,
  RenameSessionInfo
} from './types'

// Custom APIs for renderer
const api: Api = {
  getAuthCookie: () => ipcRenderer.invoke('cookie:get'),
  download: (download: DownloadInfo) => ipcRenderer.send('download:start', download),
  cancelDownload: (sessionId: string) => ipcRenderer.send('download:cancel', sessionId),
  onDownloadProgress: (callback: (progress: DownloadProgress) => void) => {
    ipcRenderer.on('download:progress', (_, progress: DownloadProgress) => {
      callback(progress)
    })
  },
  getSettings: () => ipcRenderer.invoke('settings:get'),
  setLibraryDirectory: (libraryDirectory: string) =>
    ipcRenderer.invoke('settings:setLibraryDirectory', libraryDirectory),
  setClientId: (clientId: string) => ipcRenderer.invoke('settings:setClientId', clientId),
  chooseDirectory: () => ipcRenderer.invoke('settings:chooseDirectory'),
  getLibrary: () => ipcRenderer.invoke('library:get'),
  deleteSession: (info: DeleteSessionInfo) => ipcRenderer.invoke('library:deleteSession', info),
  deleteCollection: (collectionId: string) =>
    ipcRenderer.invoke('library:deleteCollection', collectionId),
  renameCollection: (info: RenameCollectionInfo) =>
    ipcRenderer.invoke('library:renameCollection', info),
  renameSession: (info: RenameSessionInfo) => ipcRenderer.invoke('library:renameSession', info)
}

// Use `contextBridge` APIs to expose Electron APIs to
// renderer only if context isolation is enabled, otherwise
// just add to the DOM global.
if (process.contextIsolated) {
  try {
    contextBridge.exposeInMainWorld('electron', electronAPI)
    contextBridge.exposeInMainWorld('api', api)
  } catch (error) {
    console.error(error)
  }
} else {
  // @ts-ignore (define in dts)
  window.electron = electronAPI
  // @ts-ignore (define in dts)
  window.api = api
}
