export type DownloadInfo = {
  sessionId: string
  streamId: string
  streamUrl: string
  collectionId: string
  collectionName: string
  sessionName: string
  streamName: string
  timestamp: number
}

export type Progress =
  | {
      status: 'pending' | 'done' | 'error'
    }
  | {
      status: 'running'
      progress: number
    }

export type DownloadProgress = {
  sessionId: string
  streamId: string
  progress: Progress
}

export type Settings = {
  libraryDirectory: string
  clientId: string
}

type Stream = {
  id: string
  filename: string
  name: string
}

type Session = {
  id: string
  name: string
  timestamp: number
  streams: Stream[]
}

type Collection = {
  id: string
  name: string
  sessions: Session[]
}

export type Library = {
  collections: Collection[]
}

export type DeleteSessionInfo = {
  sessionId: string
  collectionId: string
}

export type RenameCollectionInfo = {
  collectionId: string
  name: string
}

export type RenameSessionInfo = {
  sessionId: string
  collectionId: string
  name: string
}

export type Api = {
  getSettings: () => Promise<Settings>
  setLibraryDirectory: (libraryDirectory: string) => Promise<void>
  setClientId: (clientId: string) => Promise<void>
  chooseDirectory: () => Promise<string | null>
  getAuthCookie: () => Promise<string | null>
  download: (download: DownloadInfo) => void
  onDownloadProgress: (callback: (data: DownloadProgress) => void) => void
  getLibrary: () => Promise<Library>
  deleteCollection: (collectionId: string) => Promise<void>
  deleteSession: (info: DeleteSessionInfo) => Promise<void>
  renameCollection: (info: RenameCollectionInfo) => Promise<void>
  renameSession: (info: RenameSessionInfo) => Promise<void>
  cancelDownload: (sessionId: string) => void
}
