import ffmpeg from 'fluent-ffmpeg'
import path from 'path'
import fs from 'fs'
import { mkdir } from 'fs/promises'
import ffmpegPath from 'ffmpeg-static'

if (ffmpegPath !== null) {
  const truePath = ffmpegPath.replace('app.asar', 'app.asar.unpacked')
  ffmpeg.setFfmpegPath(truePath)
}

type ProgressEvent = {
  frames: number
  currentFps: number
  currentKbps: number
  targetSize: number
  timemark: string
  percent: number
}

type ProgressOptions = {
  onProgress?: (progress: number) => void
  signal?: AbortSignal
}

export async function downloadStream(
  url: string,
  output: string,
  { onProgress, signal }: ProgressOptions
) {
  // create directories if not exist (promises async/await)
  const dir = path.dirname(output)
  if (!fs.existsSync(dir)) {
    await mkdir(dir, { recursive: true })
  }
  return new Promise<void>((resolve, reject) => {
    const onAbort = () => {
      console.log('Aborted download')
      command.kill('SIGKILL')
      reject(new Error('Download aborted'))
    }
    const command = ffmpeg(url)
      .on('progress', (e: ProgressEvent) => {
        if (onProgress) {
          onProgress(e.percent)
        }
      })
      .on('end', () => {
        signal?.removeEventListener('abort', onAbort)
        resolve()
      })
      .on('error', (error) => {
        signal?.removeEventListener('abort', onAbort)
        reject(error)
      })
      .outputOptions('-c:v copy')
      .outputOptions('-c:a copy')
      .outputOptions('-bsf:a aac_adtstoasc')
      .save(output)
    signal?.addEventListener('abort', onAbort)
  })
}
