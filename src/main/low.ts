import { JSONFileSync, LowSync } from '@commonify/lowdb'

export function loadLow<T>(file: string, defaultData: T) {
  const adapter = new JSONFileSync<T>(file)
  const db = new LowSync(adapter)
  db.read()
  db.data ||= defaultData
  return db
}
