import { session } from 'electron'

export async function getAspxAuthCookie() {
  const cookies = await session.defaultSession.cookies.get({
    url: 'https://cambridgelectures.cloud.panopto.eu/'
  })
  const aspxAuthCookie = cookies.find((cookie) => cookie.name === '.ASPXAUTH')
  if (aspxAuthCookie) {
    return aspxAuthCookie.value
  }
  return null
}
