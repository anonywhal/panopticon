export type DownloadInfo = {
  sessionId: string
  streamId: string
  streamUrl: string
  collectionId: string
  collectionName: string
  sessionName: string
  streamName: string
  timestamp: number
}
