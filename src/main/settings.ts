import { IpcMainInvokeEvent, app, dialog } from 'electron'
import path from 'path'
import { loadLow } from './low'
import { EventEmitter } from 'events'

type Settings = {
  libraryDirectory: string
  clientId: string
}

const defaultSettings: Settings = {
  libraryDirectory: path.join(app.getPath('videos'), 'panopticon'),
  clientId: ''
}

const dbPath = path.join(app.getPath('userData'), 'settings.json')
const db = loadLow<Settings>(dbPath, defaultSettings)

export function getSettings() {
  return db.data!
}

export const settingsEventEmitter = new EventEmitter()

export function setLibraryDirectory(_: IpcMainInvokeEvent, libraryDirectory: string) {
  db.data!.libraryDirectory = libraryDirectory
  db.write()

  // Emit an event to notify other modules about the change
  settingsEventEmitter.emit('libraryDirectoryChanged', libraryDirectory)
}

export function setClientId(_: IpcMainInvokeEvent, clientId: string) {
  db.data!.clientId = clientId
  db.write()
}

export async function chooseDirectory() {
  const result = await dialog.showOpenDialog({
    properties: ['openDirectory']
  })
  if (result.filePaths.length === 0) {
    return null
  }
  return result.filePaths[0]
}
