import { IpcMainEvent } from 'electron'
import { getSettings } from './settings'
import path from 'path'
import { downloadStream } from './ffmpeg'
import { DownloadInfo } from './types'
import { addDownload } from './library'
import { rmSync } from 'fs'

type Progress =
  | {
      status: 'pending' | 'done' | 'error'
    }
  | {
      status: 'running'
      progress: number
    }

type QueuedDownload = {
  data: DownloadInfo
  onProgress: (progress: Progress) => void
  abortController: AbortController
}

async function download({ onProgress, data, abortController }: QueuedDownload) {
  const libDir = getSettings().libraryDirectory
  const output = path.join(libDir, data.collectionId, data.sessionId, `${data.streamId}.mp4`)
  try {
    await downloadStream(data.streamUrl, output, {
      onProgress: (progress) => {
        onProgress({
          status: 'running',
          progress
        })
      },
      signal: abortController.signal
    })
    addDownload(data)
    onProgress({
      status: 'done'
    })
  } catch (error) {
    console.error(error)
    onProgress({
      status: 'error'
    })
  }
}

export class DownloadQueue {
  private queue: QueuedDownload[] = []
  private jobs: QueuedDownload[] = []

  constructor(private concurrency: number = 3) {}

  push(data: DownloadInfo, onProgress: (progress: Progress) => void) {
    const abortController = new AbortController()
    this.queue.push({
      data,
      onProgress,
      abortController
    })
    this.run()
  }

  cancel(sessionId: string) {
    const toCancel = [
      ...this.queue.filter((d) => d.data.sessionId === sessionId),
      ...this.jobs.filter((d) => d.data.sessionId === sessionId)
    ]
    toCancel.forEach((q) => q.abortController.abort())
    if (toCancel.length > 0) {
      const data = toCancel[0].data
      const libDir = getSettings().libraryDirectory
      setTimeout(() => {
        rmSync(path.join(libDir, data.collectionId, data.sessionId), {
          recursive: true,
          force: true
        })
      }, 1000)
    }
  }

  private run() {
    if (this.jobs.length >= this.concurrency) {
      return
    }

    const queued = this.queue.shift()
    if (!queued) {
      return
    }

    this.jobs.push(queued)
    download(queued).then(() => {
      this.jobs.splice(this.jobs.indexOf(queued), 1)
      this.run()
    })
  }
}

const downloadQueue = new DownloadQueue()

export function handleDownload(event: IpcMainEvent, download: DownloadInfo) {
  downloadQueue.push(download, (progress) => {
    // send progress to renderer
    event.sender.send('download:progress', {
      sessionId: download.sessionId,
      streamId: download.streamId,
      progress
    })
  })
}

export function handleCancelDownload(_event: IpcMainEvent, sessionId: string) {
  downloadQueue.cancel(sessionId)
}
