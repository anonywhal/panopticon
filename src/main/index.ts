import { app, shell, BrowserWindow, ipcMain, protocol } from 'electron'
import { join } from 'path'
import { electronApp, optimizer, is } from '@electron-toolkit/utils'
import icon from '../../resources/icon.png?asset'
import { getAspxAuthCookie } from './cookie'
import { handleCancelDownload, handleDownload } from './download'
import { chooseDirectory, getSettings, setClientId, setLibraryDirectory } from './settings'
import {
  deleteCollection,
  deleteSession,
  getLibrary,
  renameCollection,
  renameSession
} from './library'

function createWindow(): void {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 900,
    height: 670,
    show: false,
    autoHideMenuBar: true,
    titleBarStyle: 'hiddenInset',
    ...(process.platform === 'linux' ? { icon } : {}),
    webPreferences: {
      preload: join(__dirname, '../preload/index.js'),
      sandbox: false,
      webviewTag: true
    }
  })

  mainWindow.webContents.session.webRequest.onHeadersReceived((details, callback) => {
    let origin = is.dev ? 'http://localhost:5173' : 'null'
    if (details.responseHeaders?.['access-control-allow-origin']?.includes('*')) {
      origin = '*'
    }
    callback({
      responseHeaders: {
        ...details.responseHeaders,
        'access-control-allow-origin': [origin],
        'access-control-allow-credentials': 'true',
        // We use this to bypass headers
        'access-control-allow-headers': ['*']
      }
    })
  })

  mainWindow.on('ready-to-show', () => {
    mainWindow.show()
  })

  mainWindow.webContents.session.webRequest.onBeforeRequest((event, cb) => {
    const parsedUrl = new URL(event.url)
    if (parsedUrl.hostname === 'panopticon.local') {
      const hash = parsedUrl.hash
      if (is.dev && process.env['ELECTRON_RENDERER_URL']) {
        cb({
          redirectURL: process.env['ELECTRON_RENDERER_URL'] + hash
        })
      } else {
        cb({
          cancel: true
        })
        mainWindow.webContents.loadFile(join(__dirname, '../renderer/index.html'), { hash })
      }
    } else {
      cb({})
    }
  })

  mainWindow.webContents.setWindowOpenHandler((details) => {
    shell.openExternal(details.url)
    return { action: 'deny' }
  })

  // HMR for renderer base on electron-vite cli.
  // Load the remote URL for development or the local html file for production.
  if (is.dev && process.env['ELECTRON_RENDERER_URL']) {
    mainWindow.loadURL(process.env['ELECTRON_RENDERER_URL'])
  } else {
    mainWindow.loadFile(join(__dirname, '../renderer/index.html'))
  }
}

protocol.registerSchemesAsPrivileged([
  {
    scheme: 'library',
    privileges: {
      standard: true,
      secure: true,
      stream: true,
      bypassCSP: true,
      supportFetchAPI: true
    }
  }
])

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(async () => {
  // Set app user model id for windows
  electronApp.setAppUserModelId('com.electron')

  // Default open or close DevTools by F12 in development
  // and ignore CommandOrControl + R in production.
  // see https://github.com/alex8088/electron-toolkit/tree/master/packages/utils
  app.on('browser-window-created', (_, window) => {
    optimizer.watchWindowShortcuts(window)
  })

  protocol.registerFileProtocol('library', (request, callback) => {
    const { host, pathname } = new URL(request.url)
    const libraryDir = getSettings().libraryDirectory
    const filePath = join(libraryDir, `${host}${pathname}`)
    callback({
      path: filePath
    })
  })

  ipcMain.handle('settings:get', getSettings)
  ipcMain.handle('settings:setLibraryDirectory', setLibraryDirectory)
  ipcMain.handle('settings:setClientId', setClientId)
  ipcMain.handle('settings:chooseDirectory', chooseDirectory)
  ipcMain.handle('library:get', getLibrary)
  ipcMain.handle('library:deleteSession', deleteSession)
  ipcMain.handle('library:deleteCollection', deleteCollection)
  ipcMain.handle('library:renameCollection', renameCollection)
  ipcMain.handle('library:renameSession', renameSession)
  ipcMain.handle('cookie:get', getAspxAuthCookie)
  ipcMain.on('download:start', handleDownload)
  ipcMain.on('download:cancel', handleCancelDownload)

  createWindow()

  app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

// In this file you can include the rest of your app"s specific main process
// code. You can also put them in separate files and require them here.
