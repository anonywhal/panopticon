import path from 'path'
import { loadLow } from './low'
import { getSettings, settingsEventEmitter } from './settings'
import { DownloadInfo } from './types'
import { rm } from 'fs/promises'
import { IpcMainInvokeEvent } from 'electron'

type Stream = {
  id: string
  filename: string
  name: string
}

type Session = {
  id: string
  name: string
  timestamp: number
  streams: Stream[]
}

type Collection = {
  id: string
  name: string
  sessions: Session[]
}

type Library = {
  collections: Collection[]
}

const defaultLibrary: Library = {
  collections: []
}

function loadLibrary(dir: string) {
  const libraryPath = path.join(dir, 'library.json')
  return loadLow<Library>(libraryPath, defaultLibrary)
}

const libraryDir = getSettings().libraryDirectory
let db = loadLibrary(libraryDir)

settingsEventEmitter.on('libraryDirectoryChanged', (libraryDir) => {
  db = loadLibrary(libraryDir)
})

export function getLibrary() {
  return db.data!
}

export function addDownload(info: DownloadInfo) {
  // add collection if it doesn't exist
  let collection = db.data!.collections.find((collection) => collection.id === info.collectionId)
  if (!collection) {
    collection = {
      id: info.collectionId,
      name: info.collectionName,
      sessions: []
    }
    db.data!.collections.push(collection)
  }
  // add session if it doesn't exist
  let session = collection.sessions.find((session) => session.id === info.sessionId)
  if (!session) {
    session = {
      id: info.sessionId,
      name: info.sessionName,
      timestamp: info.timestamp,
      streams: []
    }
    collection.sessions.push(session)
  }
  // add stream
  const stream = {
    id: info.streamId,
    filename: `${info.streamId}.mp4`,
    name: info.streamName
  }
  session.streams.push(stream)
  db.write()
}

type DeleteSessionInfo = {
  collectionId: string
  sessionId: string
}

export async function deleteSession(_: IpcMainInvokeEvent, info: DeleteSessionInfo) {
  const collection = db.data!.collections.find((collection) => collection.id === info.collectionId)
  if (collection) {
    collection.sessions = collection.sessions.filter((session) => session.id !== info.sessionId)
    // delete the folder
    const sessionDir = path.join(libraryDir, info.collectionId, info.sessionId)
    try {
      await rm(sessionDir, { recursive: true })
      db.write()
    } catch (err) {
      console.error(err)
      // reset the db
      db.read()
    }
  }
}

export async function deleteCollection(_: IpcMainInvokeEvent, collectionId: string) {
  db.data!.collections = db.data!.collections.filter((collection) => collection.id !== collectionId)
  // delete the folder
  const collectionDir = path.join(libraryDir, collectionId)
  try {
    await rm(collectionDir, { recursive: true })
    db.write()
  } catch (err) {
    console.error(err)
    // reset the db
    db.read()
  }
}

export function renameCollection(
  _: IpcMainInvokeEvent,
  info: { collectionId: string; name: string }
) {
  const collection = db.data!.collections.find((collection) => collection.id === info.collectionId)
  if (collection) {
    collection.name = info.name
    db.write()
  }
}

export function renameSession(
  _: IpcMainInvokeEvent,
  info: {
    collectionId: string
    sessionId: string
    name: string
  }
) {
  const collection = db.data!.collections.find((collection) => collection.id === info.collectionId)
  if (collection) {
    const session = collection.sessions.find((session) => session.id === info.sessionId)
    if (session) {
      session.name = info.name
      db.write()
    }
  }
}
