import { MantineProvider } from '@mantine/core'
import { AuthProvider } from './auth/oauth'
import { RouterProvider } from '@tanstack/react-router'
import { router } from './router'
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import { ContextMenuProvider } from 'mantine-contextmenu'
import '@mantine/core/styles.css'
import 'mantine-contextmenu/styles.css'
import { ModalsProvider } from '@mantine/modals'

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      staleTime: 1000 * 60 * 5
    }
  }
})

function App(): JSX.Element {
  return (
    <QueryClientProvider client={queryClient}>
      <AuthProvider>
        <MantineProvider defaultColorScheme="dark" theme={{ primaryColor: 'cyan' }}>
          <ModalsProvider>
            <ContextMenuProvider>
              <RouterProvider router={router} />
            </ContextMenuProvider>
          </ModalsProvider>
        </MantineProvider>
      </AuthProvider>
    </QueryClientProvider>
  )
}

export default App
