import { ActionIcon, Divider, Stack, Text, TextInput, Title } from '@mantine/core'
import { useDisclosure } from '@mantine/hooks'
import { ClientHelp } from '@renderer/components/ClientHelp'
import { useSetClientId, useSetLibraryDirectory, useSettings } from '@renderer/hooks/settings'
import { IconFolder, IconHelp } from '@tabler/icons-react'

export function Settings() {
  const { data } = useSettings()
  const { mutate: mutateDir } = useSetLibraryDirectory()
  const { mutate: mutateCid } = useSetClientId()
  const [helpOpen, { open, close }] = useDisclosure()
  return (
    <>
      <Title>Settings</Title>
      <Text>Manage your preferences here.</Text>
      <Divider my="md" />
      {data && (
        <Stack maw={400}>
          <TextInput
            styles={{
              input: {
                cursor: 'default',
                color: 'var(--mantine-color-dark-2)'
              }
            }}
            readOnly
            label="Library Folder"
            value={data?.libraryDirectory}
            rightSection={
              <ActionIcon
                title="Pick Library Directory"
                variant="subtle"
                color="gray"
                onClick={() => mutateDir()}
              >
                <IconFolder size="1rem" />
              </ActionIcon>
            }
          />
          <TextInput
            label="Panopto Client ID"
            placeholder="Copy from panopto settings!"
            value={data?.clientId}
            onChange={(e) => mutateCid(e.target.value)}
            rightSection={
              <ActionIcon variant="subtle" color="gray" title="Help" onClick={open}>
                <IconHelp size="1rem" />
              </ActionIcon>
            }
          />
        </Stack>
      )}
      <ClientHelp open={helpOpen} onClose={close} />
    </>
  )
}
