import {
  ActionIcon,
  Anchor,
  Box,
  Breadcrumbs,
  Container,
  Group,
  Popover,
  SegmentedControl,
  Slider,
  Text
} from '@mantine/core'
import { Collection, Session, useSession } from '@renderer/hooks/library'
import { Link, useParams } from '@tanstack/react-router'
import { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import { AtomPlayer, NativeVideoPlayer, SyncPlayer } from '@netless/sync-player'
import {
  IconDashboard,
  IconMaximize,
  IconMinimize,
  IconPlayerPauseFilled,
  IconPlayerPlayFilled
} from '@tabler/icons-react'
import { useDisclosure, useFullscreen, useHotkeys, useHover } from '@mantine/hooks'

export function SessionPage() {
  const { collectionId, sessionId } = useParams({
    from: '/library/$collectionId/$sessionId'
  })
  const props = useSession(collectionId, sessionId)
  if (!props) return <></>
  return <LoadedSession {...props} />
}

type LoadedSessionProps = {
  collection: Collection
  session: Session
}

function LoadedSession({ session, collection }: LoadedSessionProps) {
  const baseUri = `library://${collection.id}/${session.id}`
  const [isReady, setIsReady] = useState(false)
  const { ref: boxRef, toggle, fullscreen } = useFullscreen()
  const mouseRef = useRef<HTMLDivElement>()
  const displayStreams = useMemo(() => {
    const filtered = session.streams.filter((s) => !s.name.match(/^Logo/))
    // sort by matching name Left first then Center then Right
    const sorted = filtered.sort((a, b) => {
      const index = ['Left', 'Center', 'Centre', 'Right']
      const aMatch = a.name.match(/^(Left|Center|Centre|Right)/)
      const bMatch = b.name.match(/^(Left|Center|Centre|Right)/)
      if (aMatch && bMatch) {
        return index.indexOf(aMatch[1]) - index.indexOf(bMatch[1])
      }
      if (aMatch) {
        return -1
      }
      if (bMatch) {
        return 1
      }
      return 0
    })
    return sorted
  }, [session.streams])
  const vidsRef = useRef<(HTMLVideoElement | null)[]>([])
  const syncRef = useRef<AtomPlayer>()
  const setVideo = useCallback((i: number, v: HTMLVideoElement | null) => {
    const videos = vidsRef.current
    videos[i] = v
    if (v?.duration && v.duration > duration) {
      setDuration(v.duration)
    }
    if (!(videos.length === session.streams.length)) {
      syncRef.current?.destroy()
      return
    }
    if (videos.some((v) => v === null)) {
      syncRef.current?.destroy()
      return
    }
    if (syncRef.current) {
      return
    }
    const players: AtomPlayer[] = []
    videos.forEach((v, i) => {
      if (v) {
        players.push(
          new NativeVideoPlayer({
            video: v,
            name: `video-${i}`
          })
        )
      }
    })
    const sync = new SyncPlayer({
      players: players
    })
    syncRef.current = sync
    setIsReady(true)
  }, [])
  const [isPlaying, setIsPlaying] = useState(false)
  const [current, setCurrent] = useState(displayStreams[0].id)
  const [duration, setDuration] = useState(0)
  const [position, setPosition] = useState(0)
  const [speed, setSpeed] = useState(1)
  const [showControls, setShowControls] = useState(false)
  const { ref: hoverRef, hovered } = useHover()
  // hide controls after 3 seconds
  useEffect(() => {
    if (showControls) {
      const timeout = setTimeout(() => {
        setShowControls(false)
      }, 3000)
      return () => {
        clearTimeout(timeout)
      }
    }
    return () => {}
  }, [showControls])
  // show controls on mouse move
  useEffect(() => {
    const box = mouseRef.current
    if (!box) return
    const onMouseMove = () => {
      setShowControls(true)
    }
    box.addEventListener('mousemove', onMouseMove)
    return () => {
      box.removeEventListener('mousemove', onMouseMove)
    }
  }, [])
  // intercept spacebar
  useHotkeys([
    [
      'space',
      () => {
        const sync = syncRef.current
        if (!sync) return
        if (sync.status !== 'Playing') {
          sync.play()
        }
        if (isPlaying) {
          sync.playbackRate = 0
          setIsPlaying(false)
        } else {
          sync.playbackRate = speed
          setIsPlaying(true)
        }
      }
    ],
    [
      'ArrowLeft',
      () => {
        const sync = syncRef.current
        if (!sync) return
        const currentTime = position * duration * 1000
        sync.seek(currentTime - 5000)
      }
    ],
    [
      'ArrowRight',
      () => {
        const sync = syncRef.current
        if (!sync) return
        const currentTime = position * duration * 1000
        sync.seek(currentTime + 5000)
      }
    ]
  ])
  useEffect(() => {
    const vid = vidsRef.current[0]
    if (!vid) return
    const onTimeUpdate = () => {
      setPosition(vid.currentTime / vid.duration)
      if (vid.currentTime >= vid.duration) {
        setIsPlaying(false)
      }
    }
    vid.addEventListener('timeupdate', onTimeUpdate)
    return () => {
      vid.removeEventListener('timeupdate', onTimeUpdate)
    }
  }, [isReady])
  return (
    <>
      <Breadcrumbs style={{ flexWrap: 'wrap', rowGap: '8px' }} mb="lg">
        <Anchor params={undefined as never} component={Link} to="/">
          Library
        </Anchor>
        <Anchor
          params={{ collectionId: collection.id } as never}
          component={Link}
          to="/library/$collectionId"
        >
          {collection.name}
        </Anchor>
        <Text>{session.name}</Text>
      </Breadcrumbs>
      <Container size="xl">
        <Box
          pos="relative"
          style={{
            cursor: showControls || hovered ? 'auto' : 'none'
          }}
          ref={(el) => {
            if (!el) return
            boxRef(el)
            mouseRef.current = el
          }}
        >
          {session.streams.map((stream, i) => (
            <video
              style={{
                display: stream.id === current ? 'block' : 'none',
                width: '100%',
                paddingBottom: '1px'
              }}
              key={stream.id}
              ref={(p) => setVideo(i, p)}
              src={`${baseUri}/${stream.filename}`}
            />
          ))}
          <Group
            style={{
              opacity: showControls || hovered ? 1 : 0,
              transition: 'opacity 0.3s',
              pointerEvents: showControls || hovered ? 'auto' : 'none'
            }}
            ref={hoverRef}
            // px={4}
            // py={2}
            gap={0}
            pos="absolute"
            bottom={0}
            bg="dark.9"
            left={0}
            right={0}
          >
            <ActionIcon
              radius={0}
              size="lg"
              onClick={() => {
                const sync = syncRef.current
                if (!sync) return
                if (sync.status !== 'Playing') {
                  sync.play()
                }
                if (isPlaying) {
                  sync.playbackRate = 0
                  setIsPlaying(false)
                } else {
                  sync.playbackRate = speed
                  setIsPlaying(true)
                }
              }}
            >
              {isPlaying ? (
                <IconPlayerPauseFilled size="1rem" />
              ) : (
                <IconPlayerPlayFilled size="1rem" />
              )}
            </ActionIcon>
            {/* Progress slider for video seek */}
            <Slider
              mx="xs"
              size="sm"
              style={{ flex: 1 }}
              value={position}
              max={1}
              step={0.0001}
              label={(v) => {
                const time = v * duration
                const minutes = Math.floor(time / 60)
                const seconds = Math.floor(time % 60)
                return `${minutes}:${seconds.toString().padStart(2, '0')}`
              }}
              onChange={(v) => {
                const sync = syncRef.current
                if (!sync) return
                sync.seek(v * duration * 1000)
                setPosition(v)
              }}
            />
            {displayStreams.length > 1 && (
              <SegmentedControl
                size="xs"
                px={8}
                translate="no"
                styles={{
                  root: {
                    alignSelf: 'stretch',
                    borderRadius: 0,
                    gap: 4,
                    alignItems: 'center'
                  },
                  indicator: {
                    marginBottom: 1
                  }
                }}
                color="cyan"
                value={current}
                onChange={setCurrent}
                data={displayStreams.map((stream) => ({
                  value: stream.id,
                  label: stream.name.split('_')[0]
                }))}
              />
            )}
            <SpeedController
              onChange={(s) => {
                setSpeed(s)
                const sync = syncRef.current
                if (!sync) return
                if (isPlaying) {
                  sync.playbackRate = s
                }
              }}
              value={speed}
            />
            <ActionIcon onClick={toggle} size="lg" radius={0}>
              {fullscreen ? <IconMinimize size="1rem" /> : <IconMaximize size="1rem" />}
            </ActionIcon>
          </Group>
        </Box>
      </Container>
    </>
  )
}

function SpeedController({ onChange, value }: { onChange: (v: number) => void; value: number }) {
  const [opened, { toggle, close }] = useDisclosure()
  return (
    <Popover
      opened={opened}
      clickOutsideEvents={['mouseup', 'touchend']}
      onClose={close}
      position="top"
      width={150}
      offset={0}
      withArrow
      withinPortal={false}
    >
      <Popover.Target>
        <ActionIcon
          size="lg"
          radius={0}
          title="Playback Speed"
          variant="transparent"
          onClick={toggle}
        >
          <IconDashboard size="1rem" />
        </ActionIcon>
      </Popover.Target>
      <Popover.Dropdown>
        <Slider
          size="sm"
          min={0.5}
          max={2}
          step={0.1}
          value={value}
          onChange={onChange}
          label={(v) => `${v}x`}
        />
      </Popover.Dropdown>
    </Popover>
  )
}
