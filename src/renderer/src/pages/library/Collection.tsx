import { Anchor, Breadcrumbs, Button, Stack, Text, TextInput } from '@mantine/core'
import { modals } from '@mantine/modals'
import { Session, useCollection, useDeleteSession, useRenameSession } from '@renderer/hooks/library'
import { IconMovie, IconPencil, IconTrash } from '@tabler/icons-react'
import { Link, useParams } from '@tanstack/react-router'
import { useContextMenu } from 'mantine-contextmenu'
import { useMemo, useState } from 'react'

export function Collection() {
  const { collectionId } = useParams({ from: '/library/$collectionId' })
  const collection = useCollection(collectionId)
  const sessions = useMemo(() => {
    if (!collection?.sessions) return []
    // sort in reverse order by timestamp
    return collection.sessions.sort((a, b) => a.timestamp - b.timestamp)
  }, [collection?.sessions])
  const { showContextMenu } = useContextMenu()
  const deleteSession = useDeleteSession()
  return (
    <>
      <Breadcrumbs mb="md">
        <Anchor params={undefined as never} component={Link} to="/">
          Library
        </Anchor>
        <Text>{collection?.name}</Text>
      </Breadcrumbs>
      <Stack gap={0}>
        {sessions.map((session, i) => {
          return (
            // TODO: keep track of video progress
            <Link
              style={{ textDecoration: 'none ' }}
              to="/library/$collectionId/$sessionId"
              params={{ collectionId: collectionId, sessionId: session.id }}
              key={session.id}
            >
              <Button
                fullWidth
                leftSection={<IconMovie size="1rem" />}
                variant="subtle"
                color="gray"
                justify="start"
                key={session.id}
                onContextMenu={showContextMenu([
                  {
                    key: 'rename',
                    title: 'Rename',
                    icon: <IconPencil size={16} />,
                    onClick: () => {
                      modals.open({
                        title: 'Rename Lecture',
                        children: (
                          <RenameSessionModalContent
                            session={session}
                            collectionId={collectionId}
                          />
                        )
                      })
                    }
                  },
                  {
                    key: 'delete',
                    color: 'red',
                    title: 'Delete',
                    icon: <IconTrash size={16} />,
                    onClick: () => {
                      modals.openConfirmModal({
                        title: 'Are you sure?',
                        centered: true,
                        children: (
                          <>
                            <Text size="sm">
                              <b>{session.name}</b> will be deleted.
                            </Text>
                            <Text size="sm">Are you sure you want to delete this lecture?</Text>
                          </>
                        ),
                        labels: { confirm: 'Delete', cancel: 'Cancel' },
                        confirmProps: { color: 'red' },
                        onConfirm: () => {
                          deleteSession.mutate({
                            collectionId: collectionId,
                            sessionId: session.id
                          })
                        }
                      })
                    }
                  }
                ])}
              >
                {session.name}
                <Text component="span" fw={500} size="xs" ml="1ex" c="cyan.3">
                  [{i + 1}]
                </Text>
              </Button>
            </Link>
          )
        })}
      </Stack>
    </>
  )
}

function RenameSessionModalContent({
  session,
  collectionId
}: {
  session: Session
  collectionId: string
}) {
  const [name, setName] = useState(session.name)
  const renameSession = useRenameSession()
  return (
    <>
      <Text size="sm">Enter a new name for the lecture</Text>
      <TextInput
        value={name}
        mb="md"
        onChange={(e) => setName(e.currentTarget.value)}
        placeholder={session.name}
        data-autofocus
      />
      <Button
        fullWidth
        onClick={() => {
          renameSession.mutate({ collectionId, sessionId: session.id, name })
          modals.closeAll()
        }}
      >
        Rename
      </Button>
    </>
  )
}
