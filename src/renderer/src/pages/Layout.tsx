import { AppShell, Stack, Tooltip, ActionIcon, Box, Indicator, Flex, Title } from '@mantine/core'
import { useDownloadStore } from '@renderer/hooks/download'
import { IconBooks, IconDeviceTvOld, IconDownload, IconSettings } from '@tabler/icons-react'
import { Link, Outlet, ScrollRestoration } from '@tanstack/react-router'
import { useMemo } from 'react'

export function Layout(): JSX.Element {
  const downloads = useDownloadStore((state) => state.downloads)
  const { isAny, isError, isDone } = useMemo(() => {
    const isAny = downloads.length > 0
    const isError = downloads.some((download) =>
      download.streams.some((stream) => stream.status === 'error')
    )
    const isDone = downloads.every((download) =>
      download.streams.every((stream) => stream.status === 'done')
    )
    return {
      isAny,
      isError,
      isDone
    }
  }, [downloads])
  return (
    <AppShell navbar={{ width: 66, breakpoint: 0 }} header={{ height: 36 }}>
      <AppShell.Header
        style={{
          '-webkit-app-region': 'drag'
        }}
      >
        <Flex align="center" justify="center" style={{ height: '100%' }}>
          <Title size="0.9rem">Panopticon</Title>
        </Flex>
      </AppShell.Header>
      <AppShell.Navbar p="sm">
        <Stack gap="xs" style={{ flexGrow: 1 }}>
          <Tooltip label="Library" position="right" openDelay={300}>
            <Link to="/">
              {({ isActive }) => (
                <ActionIcon variant={isActive ? 'filled' : 'subtle'} size="xl">
                  <IconBooks />
                </ActionIcon>
              )}
            </Link>
          </Tooltip>
          <Tooltip label="Resources" position="right" openDelay={300}>
            <Link to="/resources">
              {({ isActive }) => (
                <ActionIcon variant={isActive ? 'filled' : 'subtle'} size="xl">
                  <IconDeviceTvOld />
                </ActionIcon>
              )}
            </Link>
          </Tooltip>
          <Box style={{ flexGrow: 1 }} />
          <Tooltip label="Settings" position="right" openDelay={300}>
            <Link to="/settings">
              {({ isActive }) => (
                <ActionIcon variant={isActive ? 'filled' : 'subtle'} size="xl">
                  <IconSettings />
                </ActionIcon>
              )}
            </Link>
          </Tooltip>
          <Tooltip label="Downloads" position="right" openDelay={300}>
            <Indicator
              offset={7}
              disabled={!isAny}
              color={isError ? 'red' : isDone ? 'green' : 'yellow'}
            >
              <Link to="/downloads">
                {({ isActive }) => (
                  <ActionIcon variant={isActive ? 'filled' : 'subtle'} size="xl">
                    <IconDownload />
                  </ActionIcon>
                )}
              </Link>
            </Indicator>
          </Tooltip>
        </Stack>
      </AppShell.Navbar>
      <AppShell.Main>
        <Box p="sm">
          <ScrollRestoration />
          <Outlet />
        </Box>
      </AppShell.Main>
    </AppShell>
  )
}
