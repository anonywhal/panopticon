import { Title, Divider, Button, Text, Modal, Alert } from '@mantine/core'
import { useDisclosure } from '@mantine/hooks'
import { useAuth } from '@renderer/auth/oauth'
import { useAuthCookie } from '@renderer/hooks/panopto'
import { useSettings } from '@renderer/hooks/settings'
import { IconExclamationCircle } from '@tabler/icons-react'
import { Outlet } from '@tanstack/react-router'
import { useEffect, useRef } from 'react'

function WebViewComponent({ onClose }: { onClose: () => void }) {
  const ref = useRef<HTMLWebViewElement>(null)
  const { refetch } = useAuthCookie()
  useEffect(() => {
    const listener = async () => {
      const { data: cookieRes } = await refetch()
      if (cookieRes) {
        onClose()
      }
    }
    ref.current?.addEventListener('did-navigate', listener)
    return () => {
      ref.current?.removeEventListener('did-navigate', listener)
    }
  }, [])
  return (
    <>
      <webview
        src="https://cambridgelectures.cloud.panopto.eu"
        style={{ width: '100%', height: '600px', marginBottom: '10px' }}
        ref={ref}
      ></webview>
      You may close this window once you have signed in.
    </>
  )
}

export function Resources(): JSX.Element {
  const { user, login } = useAuth()
  const { data: settings } = useSettings()
  const canLogin = settings?.clientId !== '' ?? true
  const { data: cookie, isLoading } = useAuthCookie()
  const [opened, { open, close }] = useDisclosure(false)
  return (
    <>
      <Title>Panopto Resources</Title>
      <Text>Here you can find new content to download.</Text>
      {!!user && !isLoading && cookie === null && (
        <Button mt="xs" onClick={open}>
          Sign in (again!)
        </Button>
      )}
      <Divider my="md" />
      {user ? (
        <Outlet />
      ) : (
        <>
          <Button onClick={login} disabled={!canLogin}>
            Sign in to Panopto
          </Button>
          {!canLogin && (
            <Alert variant="light" mt="md" color="red" icon={<IconExclamationCircle />}>
              Please set your client ID in settings to allow sign in.
            </Alert>
          )}
        </>
      )}
      <Modal size="xl" opened={opened} onClose={close} title="Sign in to allow download">
        <WebViewComponent onClose={close} />
      </Modal>
    </>
  )
}
