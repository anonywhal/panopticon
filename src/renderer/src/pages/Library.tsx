import { ActionIcon, Button, Divider, Stack, Text, TextInput, Title } from '@mantine/core'
import { modals } from '@mantine/modals'
import {
  Collection,
  useDeleteCollection,
  useLibrary,
  useRenameCollection
} from '@renderer/hooks/library'
import { IconFolder, IconPencil, IconSearch, IconTrash, IconX } from '@tabler/icons-react'
import { Link, Outlet } from '@tanstack/react-router'
import Fuse from 'fuse.js'
import { useContextMenu } from 'mantine-contextmenu'
import { useMemo, useState } from 'react'

export function Library() {
  return (
    <>
      <Title>Library</Title>
      <Text>Browse your downloaded content.</Text>
      <Divider my="md" />
      <Outlet />
    </>
  )
}

export function LibraryContents() {
  const { data: library } = useLibrary()
  const { showContextMenu } = useContextMenu()
  const deleteCollection = useDeleteCollection()
  const [search, setSearch] = useState('')
  const filtered = useMemo(() => {
    if (!library) return undefined
    if (search === '') return library.collections
    const fuse = new Fuse(library.collections, {
      threshold: 0.4,
      keys: ['name']
    })
    return fuse.search(search).map((result) => result.item)
  }, [library, search])
  return (
    <>
      <Title>Library</Title>
      <Text>Browse your downloaded content.</Text>
      <Divider my="md" />
      <Stack gap={0}>
        <TextInput
          placeholder="Search library"
          value={search}
          onChange={(event) => setSearch(event.currentTarget.value)}
          mb="md"
          maw={400}
          leftSection={<IconSearch size="1rem" />}
          rightSection={
            search !== '' && (
              <ActionIcon variant="subtle" color="gray" onClick={() => setSearch('')}>
                <IconX size="1rem" />
              </ActionIcon>
            )
          }
        />
        {filtered?.map((collection) => {
          return (
            <Link
              style={{ textDecoration: 'none ' }}
              to="/library/$collectionId"
              params={{ collectionId: collection.id }}
              key={collection.id}
            >
              <Button
                fullWidth
                leftSection={<IconFolder size="1rem" />}
                variant="subtle"
                color="gray"
                justify="start"
                key={collection.id}
                onContextMenu={showContextMenu([
                  {
                    key: 'rename',
                    title: 'Rename',
                    icon: <IconPencil size={16} />,
                    onClick: () => {
                      modals.open({
                        title: 'Rename collection',
                        children: <RenameCollectionModalContent collection={collection} />
                      })
                    }
                  },
                  {
                    key: 'delete',
                    color: 'red',
                    title: 'Delete',
                    icon: <IconTrash size={16} />,
                    onClick: () => {
                      modals.openConfirmModal({
                        title: 'Are you sure?',
                        centered: true,
                        children: (
                          <>
                            <Text size="sm">
                              <b>{collection.name}</b> will be deleted.
                            </Text>
                            <Text size="sm">Are you sure you want to delete this collection?</Text>
                          </>
                        ),
                        labels: { confirm: 'Delete', cancel: 'Cancel' },
                        confirmProps: { color: 'red' },
                        onConfirm: () => {
                          deleteCollection.mutate(collection.id)
                        }
                      })
                    }
                  }
                ])}
              >
                {collection.name}
              </Button>
            </Link>
          )
        })}
      </Stack>
    </>
  )
}

function RenameCollectionModalContent({ collection }: { collection: Collection }) {
  const [name, setName] = useState(collection.name)
  const renameCollection = useRenameCollection()
  return (
    <>
      <Text size="sm">Enter a new name for the collection</Text>
      <TextInput
        value={name}
        mb="md"
        onChange={(e) => setName(e.currentTarget.value)}
        placeholder={collection.name}
        data-autofocus
      />
      <Button
        fullWidth
        onClick={() => {
          renameCollection.mutate({ collectionId: collection.id, name })
          modals.closeAll()
        }}
      >
        Rename
      </Button>
    </>
  )
}
