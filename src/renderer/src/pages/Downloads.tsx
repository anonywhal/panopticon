import { ActionIcon, Box, Divider, Progress, Stack, Text, Title } from '@mantine/core'
import { DownloadWithProgress, useCancelDownload, useDownloadStore } from '@renderer/hooks/download'
import { IconX } from '@tabler/icons-react'
import { useQueryClient } from '@tanstack/react-query'
import { useEffect, useMemo } from 'react'

export function Downloads() {
  const downloads = useDownloadStore((state) => state.downloads)
  const queryClient = useQueryClient()
  useEffect(() => {
    queryClient.invalidateQueries({
      queryKey: ['library']
    })
  }, [downloads])

  return (
    <>
      <Title>Downloads</Title>
      <Text>Manage your ongoing and completed downloads.</Text>
      <Divider my="md" />
      <Stack gap="lg" px="md">
        {downloads.map((download) => (
          <DownloadRow key={download.id} download={download} />
        ))}
      </Stack>
    </>
  )
}

type DownloadRowProps = {
  download: DownloadWithProgress
}

function DownloadRow({ download }: DownloadRowProps) {
  const removeDownload = useDownloadStore((state) => state.removeDownload)
  const progress = useMemo(() => {
    const sumProgress = download.streams.reduce((acc, stream) => {
      return acc + stream.progress
    }, 0)
    return sumProgress / download.streams.length
  }, [download])
  const { isError, isDone, isPending, isRunning } = useMemo(() => {
    const isError = download.streams.some((stream) => stream.status === 'error')
    const isDone = download.streams.every((stream) => stream.status === 'done')
    const isPending = download.streams.every((stream) => stream.status === 'pending')
    const isRunning = download.streams.some((stream) => stream.status === 'running')
    return {
      isError,
      isDone,
      isPending,
      isRunning
    }
  }, [download])
  const cancel = useCancelDownload()
  return (
    <Box pos="relative">
      <ActionIcon
        // TODO: implement
        variant="subtle"
        color="red"
        pos="absolute"
        top={0}
        right={0}
        title={isDone || isError ? 'Remove Download' : 'Cancel Download'}
        onClick={() => {
          if (isDone || isError) {
            removeDownload(download.id)
          } else {
            cancel.mutate(download.id)
          }
        }}
      >
        <IconX size={18} />
      </ActionIcon>
      <Text fw="bold">{download.name}</Text>
      <Text size="xs">{download.id}</Text>
      <Progress
        value={progress}
        animated={isRunning}
        size="sm"
        mt="xs"
        color={isError ? 'red' : isDone ? 'green' : isPending ? 'gray' : 'cyan'}
      />
    </Box>
  )
}
