import {
  ActionIcon,
  Anchor,
  Breadcrumbs,
  Button,
  Card,
  Checkbox,
  Group,
  Image,
  SimpleGrid,
  Skeleton,
  Text
} from '@mantine/core'
import { useListState } from '@mantine/hooks'
import { useDownload } from '@renderer/hooks/download'
import { useDownloadedIds } from '@renderer/hooks/library'
import { Session, useAuthCookie, useFolderContents } from '@renderer/hooks/panopto'
import { IconDownload, IconExternalLink } from '@tabler/icons-react'
import { Link, useParams } from '@tanstack/react-router'
import dayjs from 'dayjs'
import duration from 'dayjs/plugin/duration'

dayjs.extend(duration)

export function FolderContents() {
  const cookieResult = useAuthCookie()
  const canDownload = !!cookieResult.data
  const download = useDownload()
  const [selected, handlers] = useListState<Session>([])
  const { folderId } = useParams({ from: '/resources/folders/$folderId' })
  const { data, isLoading } = useFolderContents(folderId)
  // TODO: include ongoing downloads in this
  const downloadedIds = useDownloadedIds()
  if (!data || isLoading) {
    return <Skeleton height={200} width={300} />
  }
  return (
    <>
      <Group justify="space-between" mb="md">
        <Breadcrumbs>
          <Anchor params={undefined as never} component={Link} to="/resources">
            Resources
          </Anchor>
          <Text>{data?.folder.Name}</Text>
        </Breadcrumbs>
        <Group gap="md" h="xl">
          {selected.length > 0 && (
            <Button
              onClick={() => {
                download.mutate(selected.filter((s) => !downloadedIds.includes(s.Id)))
                handlers.setState([])
              }}
              disabled={!canDownload}
              size="xs"
              leftSection={<IconDownload size="1rem" />}
            >
              Download ({selected.length})
            </Button>
          )}
          <Checkbox
            checked={selected.length === data?.sessions.length}
            indeterminate={selected.length > 0 && selected.length < data?.sessions.length}
            onChange={(e) => {
              if (e.target.checked || e.target.indeterminate) {
                handlers.setState(data?.sessions ?? [])
              } else {
                handlers.setState([])
              }
            }}
            labelPosition="left"
            label={selected.length === data?.sessions.length ? 'Deselect All' : 'Select All'}
          />
        </Group>
      </Group>
      <SimpleGrid cols={{ base: 2, sm: 3, lg: 4 }}>
        {data?.sessions.map((session) => (
          <Card key={session.Id} bg={selected.includes(session) ? 'dark.4' : 'dark.6'}>
            <Card.Section>
              <Checkbox
                pos="absolute"
                top={6}
                left={6}
                checked={selected.includes(session)}
                onChange={(e) => {
                  if (e.target.checked) {
                    handlers.append(session)
                  } else {
                    handlers.filter((s) => s.Id !== session.Id)
                  }
                }}
              ></Checkbox>
              <Image mah="100" src={session.Urls.ThumbnailUrl} alt="Thumbnail" />
            </Card.Section>
            <Text
              mt="sm"
              size="sm"
              fw="bold"
              style={{
                whiteSpace: 'nowrap',
                overflow: 'hidden',
                textOverflow: 'ellipsis'
              }}
            >
              {session.Name}
            </Text>
            <Group justify="space-between" mt={3}>
              <Text size="xs" c="gray.5">
                {dayjs(session.StartTime).format('MMM D, YYYY')}
              </Text>
              <Text size="xs" c="gray.5">
                {dayjs.duration(session.Duration, 's').format('H:mm:ss')}
              </Text>
            </Group>
            <Group mt="xs" justify="space-between" gap="xs">
              <ActionIcon
                disabled={!canDownload || downloadedIds.includes(session.Id)}
                onClick={() => download.mutate([session])}
                variant="subtle"
                size="sm"
              >
                <IconDownload size="1rem" />
              </ActionIcon>
              <ActionIcon
                component="a"
                href={session.Urls.ViewerUrl}
                target="_blank"
                variant="subtle"
                size="sm"
              >
                <IconExternalLink size="1rem" />
              </ActionIcon>
            </Group>
          </Card>
        ))}
      </SimpleGrid>
    </>
  )
}
