import { ActionIcon, Button, Stack, Text, TextInput } from '@mantine/core'
import { modals } from '@mantine/modals'
import { useFolders } from '@renderer/hooks/panopto'
import { IconClipboardText, IconFolder, IconSearch, IconX } from '@tabler/icons-react'
import { Link, useNavigate } from '@tanstack/react-router'
import Fuse from 'fuse.js'
import { useMemo, useState } from 'react'

export function Folders() {
  const { data } = useFolders()
  const [search, setSearch] = useState('')
  const filtered = useMemo(() => {
    if (!data) return undefined
    if (search === '') return data.Results
    const fuse = new Fuse(data.Results, {
      threshold: 0.4,
      keys: ['Name']
    })
    return fuse.search(search).map((result) => result.item)
  }, [data, search])
  const navigate = useNavigate()
  const goToCustom = (folderId: string) => {
    navigate({
      to: '/resources/folders/$folderId',
      params: {
        folderId: folderId
      }
    })
  }

  return (
    <Stack gap={0}>
      <TextInput
        placeholder="Search folders"
        value={search}
        onChange={(event) => setSearch(event.currentTarget.value)}
        mb="md"
        maw={400}
        leftSection={<IconSearch size="1rem" />}
        rightSection={
          search !== '' && (
            <ActionIcon variant="subtle" color="gray" onClick={() => setSearch('')}>
              <IconX size="1rem" />
            </ActionIcon>
          )
        }
      />
      <Button
        pos="fixed"
        bottom={16}
        right={16}
        style={{ zIndex: 100 }}
        leftSection={<IconClipboardText size="1rem" />}
        onClick={() => {
          modals.open({
            title: 'Paste a link to a collection',
            children: <PasteLinkModalContent onSubmit={goToCustom} />
          })
        }}
      >
        Paste a link
      </Button>
      {filtered?.map((folder) => {
        return (
          <Link
            style={{ textDecoration: 'none ' }}
            to="/resources/folders/$folderId"
            params={{ folderId: folder.Id }}
            key={folder.Id}
          >
            <Button
              fullWidth
              leftSection={<IconFolder size="1rem" />}
              variant="subtle"
              color="gray"
              justify="start"
              key={folder.Id}
            >
              {folder.Name}
            </Button>
          </Link>
        )
      })}
    </Stack>
  )
}

function PasteLinkModalContent({ onSubmit }: { onSubmit: (folderId: string) => void }) {
  const [link, setLink] = useState('')
  const folderId = useMemo(() => {
    try {
      const url = new URL(link)
      const re = /folderID=%22([0-9a-f-]+)%22/
      const matches = re.exec(url.hash)
      if (!matches) return null
      if (matches.length < 2) return null
      return matches[1]
    } catch {
      return null
    }
  }, [link])
  return (
    <>
      <Text size="sm">Enter the panopto link to the collection</Text>
      <TextInput
        value={link}
        mb="md"
        onChange={(e) => setLink(e.currentTarget.value)}
        placeholder={'https://cambridgelectures.cloud.panopto.eu/...'}
        data-autofocus
        error={link !== '' && folderId === null ? 'Invalid link' : null}
      />
      <Button
        fullWidth
        disabled={folderId === null}
        onClick={() => {
          // renameCollection.mutate({ collectionId: collection.id, name })
          modals.closeAll()
          if (folderId !== null) {
            onSubmit(folderId)
          }
        }}
      >
        View content
      </Button>
    </>
  )
}
