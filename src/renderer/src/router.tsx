import { RootRoute, Route, Router, createMemoryHistory } from '@tanstack/react-router'
import { Layout } from './pages/Layout'
import { Resources } from './pages/Resources'
import { Folders } from './pages/resources/Folders'
import { FolderContents } from './pages/resources/FolderContents'
import { Downloads } from './pages/Downloads'
import { Settings } from './pages/Settings'
import { Library, LibraryContents } from './pages/Library'
import { Collection } from './pages/library/Collection'
import { SessionPage } from './pages/library/Session'

const memoryHistory = createMemoryHistory({
  initialEntries: ['/'] // Pass your initial url
})

const rootRoute = new RootRoute({
  component: Layout
})

const homeRoute = new Route({
  getParentRoute: () => rootRoute,
  path: '/',
  component: LibraryContents
})

const libraryRoute = new Route({
  getParentRoute: () => rootRoute,
  path: '/library',
  component: Library
})

const collectionRoute = new Route({
  getParentRoute: () => libraryRoute,
  path: '/$collectionId',
  component: Collection
})

const sessionRoute = new Route({
  getParentRoute: () => libraryRoute,
  path: '/$collectionId/$sessionId',
  component: SessionPage
})

const resourcesRoute = new Route({
  getParentRoute: () => rootRoute,
  path: '/resources',
  component: Resources
})

const foldersRoute = new Route({
  getParentRoute: () => resourcesRoute,
  path: '/',
  component: Folders
})
const folderContentsRoute = new Route({
  getParentRoute: () => resourcesRoute,
  path: '/folders/$folderId',
  component: FolderContents
})

const downloadsRoute = new Route({
  getParentRoute: () => rootRoute,
  path: '/downloads',
  component: Downloads
})

const settingsRoute = new Route({
  getParentRoute: () => rootRoute,
  path: '/settings',
  component: Settings
})

const routeTree = rootRoute.addChildren([
  homeRoute,
  libraryRoute.addChildren([collectionRoute.addChildren([sessionRoute])]),
  resourcesRoute.addChildren([foldersRoute, folderContentsRoute]),
  downloadsRoute,
  settingsRoute
])
export const router = new Router({ routeTree, history: memoryHistory })

declare module '@tanstack/react-router' {
  interface Register {
    router: typeof router
  }
}
