import { JwtPayload, jwtDecode } from 'jwt-decode'
import { createContext, useCallback, useContext, useEffect, useState } from 'react'

export type AuthConfig = {
  authorizationUrl: string
  redirectUri: string
  scopes: string[]
}

type AuthState = {
  token?: string
  user?: JwtPayload & {
    name: string
  }
}

type Auth = AuthState & {
  login: () => void
}

const AuthStateContext = createContext<Auth>(null as never)

async function initiateAuth(config: AuthConfig): Promise<void> {
  const clientId = (await window.api.getSettings()).clientId
  const url = new URL(config.authorizationUrl)
  url.searchParams.append('client_id', clientId)
  url.searchParams.append('redirect_uri', config.redirectUri)
  url.searchParams.append('response_type', 'token')
  url.searchParams.append('scope', config.scopes.join(' '))
  window.location.href = url.toString()
}

function finishAuth(): AuthState | undefined {
  const existing = localStorage.getItem('auth')
  if (existing) {
    const state: AuthState = JSON.parse(existing)
    const exp = state.user?.exp ?? 0
    if (exp > Date.now() / 1000) {
      return state
    }
  }
  const hash = window.location.hash
  const params = new URLSearchParams(hash.substring(1))
  if (!params.has('access_token')) {
    return undefined
  }
  const token = params.get('access_token') ?? ''
  const user = jwtDecode(token)
  localStorage.setItem('auth', JSON.stringify({ token, user }))
  return { token, user: user as JwtPayload & { name: string } }
}

export function AuthProvider({ children }: { children: React.ReactNode }): JSX.Element {
  const [state, setState] = useState<AuthState>({})
  useEffect(() => {
    try {
      const authState = finishAuth()
      if (authState) {
        setState(authState)
      }
    } catch (e) {
      console.log(e)
    }
  }, [])
  const login = useCallback(() => {
    initiateAuth({
      authorizationUrl:
        'https://cambridgelectures.cloud.panopto.eu/Panopto/oauth2/connect/authorize',
      redirectUri: 'https://panopticon.local/callback',
      scopes: ['offline_access', 'api']
    })
  }, [])

  return (
    <AuthStateContext.Provider value={{ ...state, login }}>{children}</AuthStateContext.Provider>
  )
}

export function useAuth(): Auth {
  return useContext(AuthStateContext)
}
