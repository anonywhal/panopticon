import { useQuery } from '@tanstack/react-query'
import { useAxios } from './useAxios'

type Folder = {
  Name: string
  Id: string
}

type FoldersResult = {
  Results: Folder[]
}

export type Session = {
  Name: string
  Duration: number
  Id: string
  StartTime: string
  Urls: {
    ThumbnailUrl: string
    ViewerUrl: string
  }
}

type FolderResult = {
  Folder: Folder
  Results: Session[]
}

export function useFolders() {
  const axios = useAxios()
  return useQuery({
    queryKey: ['folders'],
    queryFn: async () => {
      const { data } = await axios.get<FoldersResult>(
        '/folders/00000000-0000-0000-0000-000000000000/children'
      )
      return data
    }
  })
}

export function useAuthCookie() {
  return useQuery({
    queryKey: ['authCookie'],
    queryFn: async () => {
      const cookie = await window.api.getAuthCookie()
      return cookie
    },
    retry: false,
    staleTime: 0
  })
}

export function useFolderContents(folderId: string) {
  const axios = useAxios()
  return useQuery({
    queryKey: ['folders', folderId],
    queryFn: async () => {
      const { data: folder } = await axios.get<Folder>(`/folders/${folderId}`)
      const { data } = await axios.get<FolderResult>(`/folders/${folderId}/sessions`)
      data.Results.sort((a, b) => {
        return new Date(b.StartTime).getTime() - new Date(a.StartTime).getTime()
      })
      return { folder, sessions: data.Results }
    }
  })
}
