import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query'
import { useMemo } from 'react'
import { useDownloadStore } from './download'

export function useLibrary() {
  return useQuery({
    queryKey: ['library'],
    queryFn: () => {
      return window.api.getLibrary()
    }
  })
}

export function useDownloadedIds() {
  const { data } = useLibrary()
  const downloads = useDownloadStore((state) => state.downloads.map((d) => d.id))
  return useMemo(() => {
    if (!data) {
      return []
    }
    const ids: string[] = []
    for (const collection of data.collections) {
      for (const session of collection.sessions) {
        ids.push(session.id)
      }
    }
    // return both downloaded and downloading
    return [...ids, ...downloads]
  }, [data, downloads])
}

export function useCollection(id: string) {
  const { data } = useLibrary()
  return useMemo(() => {
    if (!data) {
      return
    }
    return data.collections.find((c) => c.id === id)
  }, [data, id])
}

type Stream = {
  id: string
  filename: string
  name: string
}

export type Session = {
  id: string
  name: string
  streams: Stream[]
}

export type Collection = {
  id: string
  name: string
  sessions: Session[]
}

export function useSession(collectionId: string, sessionId: string) {
  const collection = useCollection(collectionId)
  return useMemo(() => {
    if (!collection) {
      return
    }
    const session = collection.sessions.find((s) => s.id === sessionId)
    if (!session) {
      return
    }
    return {
      collection,
      session
    }
  }, [collection, sessionId])
}

type DeleteSessionInfo = {
  sessionId: string
  collectionId: string
}

export function useDeleteSession() {
  const queryClient = useQueryClient()
  return useMutation({
    mutationKey: ['library', 'deleteSession'],
    mutationFn: (info: DeleteSessionInfo) => {
      return window.api.deleteSession(info)
    },
    onSuccess: () => {
      queryClient.invalidateQueries({
        queryKey: ['library']
      })
    }
  })
}

export function useDeleteCollection() {
  const queryClient = useQueryClient()
  return useMutation({
    mutationKey: ['library', 'deleteCollection'],
    mutationFn: (collectionId: string) => {
      return window.api.deleteCollection(collectionId)
    },
    onSuccess: () => {
      queryClient.invalidateQueries({
        queryKey: ['library']
      })
    }
  })
}

type RenameCollectionInfo = {
  collectionId: string
  name: string
}

export function useRenameCollection() {
  const queryClient = useQueryClient()
  return useMutation({
    mutationKey: ['library', 'renameCollection'],
    mutationFn: (info: RenameCollectionInfo) => {
      return window.api.renameCollection(info)
    },
    onSuccess: () => {
      queryClient.invalidateQueries({
        queryKey: ['library']
      })
    }
  })
}

type RenameSessionInfo = {
  sessionId: string
  collectionId: string
  name: string
}

export function useRenameSession() {
  const queryClient = useQueryClient()
  return useMutation({
    mutationKey: ['library', 'renameSession'],
    mutationFn: (info: RenameSessionInfo) => {
      return window.api.renameSession(info)
    },
    onSuccess: () => {
      queryClient.invalidateQueries({
        queryKey: ['library']
      })
    }
  })
}
