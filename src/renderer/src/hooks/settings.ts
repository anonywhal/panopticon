import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query'

export function useSettings() {
  return useQuery({
    queryKey: ['settings'],
    queryFn: () => {
      return window.api.getSettings()
    }
  })
}

export function useSetLibraryDirectory() {
  const queryClient = useQueryClient()
  return useMutation({
    mutationKey: ['setLibraryDirectory'],
    mutationFn: async () => {
      const libraryDirectory = await window.api.chooseDirectory()
      if (libraryDirectory === null) {
        throw new Error('No directory selected')
      }
      return window.api.setLibraryDirectory(libraryDirectory)
    },
    onSuccess: () => {
      queryClient.invalidateQueries({
        queryKey: ['settings']
      })
      queryClient.invalidateQueries({
        queryKey: ['library']
      })
    }
  })
}

export function useSetClientId() {
  const queryClient = useQueryClient()
  return useMutation({
    mutationKey: ['setClientId'],
    mutationFn: async (clientId: string) => {
      return window.api.setClientId(clientId)
    },
    onSuccess: () => {
      queryClient.invalidateQueries({
        queryKey: ['settings']
      })
    }
  })
}
