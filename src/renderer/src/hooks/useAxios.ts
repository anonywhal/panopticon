import { useAuth } from '@renderer/auth/oauth'
import axios from 'axios'

export function useAxios() {
  const { token } = useAuth()
  const axiosInstance = axios.create({
    baseURL: 'https://cambridgelectures.cloud.panopto.eu/Panopto/api/v1',
    headers: {
      Authorization: token ? `Bearer ${token}` : ''
    }
  })
  return axiosInstance
}
