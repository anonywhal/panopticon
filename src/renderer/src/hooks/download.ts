import { AxiosInstance } from 'axios'
import { useAxios } from './useAxios'
import { useMutation } from '@tanstack/react-query'
import { create } from 'zustand'
import { immer } from 'zustand/middleware/immer'
import { Session } from './panopto'
import dayjs from 'dayjs'

type DeliveryInfo = {
  Delivery: {
    Streams: {
      Name: string
      StreamUrl: string
      PublicID: string
      Variants: {
        Url: string
      }[]
    }[]
    SessionGroupPublicID: string
    SessionGroupLongName: string
    SessionName: string
    PublicID: string
  }
}

type Stream = {
  name: string
  url: string
  id: string
}

type StreamWithProgress = Stream & {
  progress: number
  status: 'pending' | 'done' | 'error' | 'running'
}

type Download = {
  name: string
  id: string
  collectionId: string
  collectionName: string
  sessionName: string
  streams: Stream[]
  timestamp: number
}

export type DownloadWithProgress = {
  name: string
  id: string
  collectionId: string
  collectionName: string
  sessionName: string
  streams: StreamWithProgress[]
}

type DownloadState = {
  downloads: DownloadWithProgress[]
  addDownload: (download: Download) => void
  removeDownload: (id: string) => void
}

export const useDownloadStore = create<DownloadState>()(
  immer((set) => ({
    downloads: [],
    addDownload: (download) => {
      set((state) => {
        state.downloads.push({
          ...download,
          streams: download.streams.map((stream) => ({
            ...stream,
            status: 'pending',
            progress: 0
          }))
        })
      })
      download.streams.forEach((stream) => {
        window.api.download({
          sessionId: download.id,
          streamId: stream.id,
          streamUrl: stream.url,
          collectionId: download.collectionId,
          collectionName: download.collectionName,
          sessionName: download.sessionName,
          streamName: stream.name,
          timestamp: download.timestamp
        })
      })
    },
    removeDownload: (id) =>
      set((state) => {
        state.downloads.splice(
          state.downloads.findIndex((download) => download.id === id),
          1
        )
      })
  }))
)

window.api.onDownloadProgress((data) => {
  useDownloadStore.setState((state) => {
    const download = state.downloads.find((download) => download.id === data.sessionId)
    if (!download) {
      return
    }
    const stream = download.streams.find((stream) => stream.id === data.streamId)
    if (!stream) {
      return
    }
    stream.status = data.progress.status
    switch (data.progress.status) {
      case 'running':
        stream.progress = data.progress.progress
        break
      case 'done':
        stream.progress = 100
        break
      case 'pending':
      case 'error':
        break
    }
  })
})

async function fetchSession(sessionId: string, axios: AxiosInstance) {
  const payload = {
    deliveryId: sessionId,
    responseType: 'json'
  }
  const formData = new FormData()
  Object.keys(payload).forEach((key) => {
    formData.append(key, payload[key])
  })
  const { data } = await axios.post<DeliveryInfo>(`/Pages/Viewer/DeliveryInfo.aspx`, formData, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      Authorization: null
    },
    baseURL: 'https://cambridgelectures.cloud.panopto.eu/Panopto',
    withCredentials: true
  })
  return data
}

export function useDownload() {
  const axios = useAxios()
  const addDownload = useDownloadStore((state) => state.addDownload)
  return useMutation({
    mutationKey: ['download'],
    mutationFn: async (sessions: Session[]) => {
      const promises = sessions.map((s) =>
        fetchSession(s.Id, axios).then((data) => ({ result: data, session: s }))
      )
      const results = await Promise.all(promises)
      results.forEach(({ result, session }) => {
        const download = {
          name: result.Delivery.SessionName,
          id: result.Delivery.PublicID,
          collectionId: result.Delivery.SessionGroupPublicID,
          collectionName: result.Delivery.SessionGroupLongName,
          sessionName: result.Delivery.SessionName,
          timestamp: dayjs(session.StartTime).unix(),
          streams: result.Delivery.Streams.map((stream) => ({
            name: stream.Name,
            url: stream.Variants[0].Url,
            id: stream.PublicID
          }))
        }
        addDownload(download)
      })
    }
  })
}

export function useCancelDownload() {
  return useMutation({
    mutationKey: ['cancelDownload'],
    mutationFn: async (sessionId: string) => {
      window.api.cancelDownload(sessionId)
    }
  })
}
