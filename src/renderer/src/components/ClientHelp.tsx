import { Anchor, List, Modal, Popover, Text, Title } from '@mantine/core'
import { useDisclosure } from '@mantine/hooks'

type Props = {
  open: boolean
  onClose: () => void
}

function ImgTT({ children, src }: { children: React.ReactNode; src: string }) {
  const [opened, { close, open }] = useDisclosure(false)
  return (
    <Popover shadow="md" opened={opened} withArrow position="bottom">
      <Popover.Target>
        <span
          onMouseEnter={open}
          onMouseLeave={close}
          style={{
            cursor: 'help',
            borderBottom: '1px dashed'
          }}
        >
          {children}
        </span>
      </Popover.Target>
      <Popover.Dropdown style={{ pointerEvents: 'none' }}>
        <img src={src} alt="Screenshot" style={{ maxWidth: '100%' }} />
      </Popover.Dropdown>
    </Popover>
  )
}

function ClientHelpText() {
  return (
    <>
      <Text>To get a Panopto Client ID for use in this application, you have two options:</Text>
      <Text mb="md">
        The first option is to get someone else to send you theirs; the second option is to get your
        own, but this requires a few steps.
      </Text>
      <List type="ordered">
        <List.Item>
          First, go to the{' '}
          <Anchor
            href="https://cambridgelectures.cloud.panopto.eu"
            target="_blank"
            rel="noopener noreferrer"
          >
            Panopto site
          </Anchor>{' '}
          and log in to your account.
        </List.Item>
        <List.Item>
          Next, <ImgTT src="topright.png">click on your name</ImgTT> in the top right corner, and
          select <b>User Settings</b> from the dropdown menu.
        </List.Item>
        <List.Item>
          Select the{' '}
          <ImgTT src="apiclients.png">
            <b>API Clients</b> tab
          </ImgTT>
          , and then click <b>Create a new API Client</b>.
        </List.Item>
        <List.Item>
          Fill in the <ImgTT src="clientinfo.png">new API Client form</ImgTT> with the following
          information, then click <b>Create API Client</b>:
          <List>
            <List.Item>
              <b>Name:</b> Any name you like (perhaps &quot;Panopticon Client&quot;)
            </List.Item>
            <List.Item>
              <b>Client Type:</b> JavaScript Web Application
            </List.Item>
            <List.Item>
              <b>CORS Origin URL:</b> <code>https://panopticon.local/</code>
            </List.Item>
            <List.Item>
              <b>Redirect URL:</b> <code>https://panopticon.local/callback</code>
            </List.Item>
            <List.Item>
              <b>Post Logout Redirect URL:</b> <code>https://panopticon.local/callback</code>
            </List.Item>
          </List>
        </List.Item>
        <List.Item>
          Copy the <b>Client ID</b> from the new API Client you just created, and paste it into the
          settings page in this application.
        </List.Item>
      </List>
    </>
  )
}

export function ClientHelp({ open, onClose }: Props) {
  return (
    <Modal.Root fullScreen opened={open} onClose={onClose}>
      <Modal.Content mt={36}>
        <Modal.Header>
          <Modal.Title>
            <Title order={3}>Help: How to get a Client ID</Title>
          </Modal.Title>
          <Modal.CloseButton />
        </Modal.Header>
        <Modal.Body>
          <ClientHelpText />
        </Modal.Body>
      </Modal.Content>
    </Modal.Root>
  )
}
